// Always use an IIFE, i.e., (function() {})();
(function () {
    var app = angular.module("todoApp",[]);
    
    app.controller("TodoCtrl",["$http", TodoCtrl]);
    
//<<<<<<< HEAD
//=======
    /*[David, 1/9]: Added sorting/filtering functionality
    added attribute priority for sorting],
    filter function and defaulting sort needs fixing*/
     
//>>>>>>> master
    function TodoCtrl($http) {
        var vm = this;

        /*INSTANTIATE VARIABLES/FUNCTIONS*/
        //todo and donedoto arrays linked to "Incomplete Task" and "Completed Tasks" respectively
        vm.todo = [];
        vm.doneTodo = [];
        //task object for each task created
        vm.newTodo = {};
        //remove, submit, status change functions
        vm.remove = remove;
        vm.changeStatus = changeStatus;
        //search, propertyName, reverse functions (reverse == false) -> ascending
        vm.search = "";
        vm.propertyName = "-id";
        //reverse false ascending
        vm.reverse = false;
        vm.tabStatus = 1;
        // [WW] instatiate editTask object
        vm.editTask = editTask;
        //===================

        //Generates priorityID
        vm.makePriorityID = function(newTodo){
            if (newTodo.priority=="HIGH"){
                vm.newTodo.priorityID = 1
            } else if (newTodo.priority=="NORMAL") {
                vm.newTodo.priorityID = 2
            } else {
                vm.newTodo.priorityID = 3
            }
        };

        //Creates new task and pushes into Not Completed items. POST to server side.
        vm.submit = function(){
            vm.makePriorityID(vm.newTodo);
            vm.todo.push(vm.newTodo);
            console.log("Submitting task...: " + vm.newTodo);
            $http.post("/newtask", vm.newTodo)
                .then(function(result){
                console.log("Success!");
                }).catch(function(e){
                    console.log(e);
                });  
            vm.newTodo= {};
        }


        vm.sortBy = function(propertyName){
            vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse: false;
            vm.propertyName = propertyName;
        }

        function remove(doneStatus, index) {
            if(doneStatus) {
                console.log("Deleting task...: " + vm.newTodo);
                vm.doneTodo.splice(index, 1);
                $http.post("/deletetask", vm.doneTodo)
                .then(function(result){
                    console.log("Success!");
                }).catch(function(e){
                    console.log(e);
                });
            }
            else{
                console.log("Deleting task...: " + vm.newTodo);
                vm.todo.splice(index, 1);
                $http.post("/deletetask", vm.doneTodo)
                .then(function(result){
                    console.log("Success!");
                }).catch(function(e){
                    console.log(e);
                });
            }
        };
        
        //doneStatus == false, goes to todo array.
        //doneStatus == true, goes to donetodo array
        //NOTE: !doneStatus == true
        function changeStatus(doneStatus, index) {
            if(!doneStatus) {
                vm.doneTodo = vm.doneTodo.concat(vm.todo.splice(index, 1));
            }

            else {
                vm.todo = vm.todo.concat(vm.doneTodo.splice(index, 1));
                }
        };

        // [WW] This function allows tasks to be editted when the edit
        // task button is clicked among the incomplete tasks thumbnails. 
        function editTask(doneStatus, index) {

          // this fetches the fields immediately to the input areas
          vm.newTodo.taskName = vm.todo[index].taskName;
          vm.newTodo.priority = vm.todo[index].priority;
          vm.newTodo.dueDate = vm.todo[index].dueDate;

          // this removes the entry immediately from the todo array
          vm.todo.splice(index, 1);
  
        };
    };   
})();


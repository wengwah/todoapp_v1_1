/**
 * toDoApp
 * Server side code
 * Author(s): Tan Cheen Chong,
 * Last updated: 31/8/2017
 * 
 * server calls
 * POST /newtask
 * POST /updatetask
 * POST /deletetask
 * 
 */

// use strict JS coding practice
"use strict";

// Create a local express instance and assign to toDoApp
var express = require("express");
//Change "toDoApp" to "app" [David]
var toDoApp = express();

// Declare dependencies: body parser which converts URL into JSON format
var bodyParser = require("body-parser");
toDoApp.use(bodyParser.urlencoded({ extended: false })); 
toDoApp.use(bodyParser.json()); 
// Declare where all other dependency files reside
toDoApp.use(express.static(__dirname + "/../client/"));

// Set server NODE_PORT to environment if exist, otherwise default to port 3000
const NODE_PORT = process.env.PORT || 3000;

// Declare global variable to access active taskObject being manipulated
// tasksArray will be holding all the tasks being manipulated: active, completed
var tasksArray = [];
// taskId maintains a running number to give each newly-created task a unique identifier
// starts from 0 every time the server is restarted
var taskId = 0;

var taskObject ="";

// POST call to add a new task into the system
toDoApp.post("/newtask", function(req, res) {
    // taskObject holds the active task being added
    var taskObject = req.body;
    console.log("Received task data for addition: " + JSON.stringify(req.body));
    console.log("ID being assigned to this task: " + taskId)
    res.json(taskObject);

    // add new task to the top of tasksArray, or if empty, as the first task object
    tasksArray.push(taskObject);

    // increment the taskId in preparation for the next task addition
    taskId++;
    // reply back to client with a success status + the active task -- for subsequent manipulation
    // REMOVE COMMENT TAG BELOW!!!!
    //res.status(200).json(taskObject);

});

// POST call to update details of a task, including status and deadline
toDoApp.post("updatetask", function(req, res) {
    // taskObject holds the active task being updated
    var taskObject = req.body;
    // taskIdToDelete stores the ID of the  task to be updated
    var taskIdToDelete = taskObject.Id;
    var i=0;

    console.log("Received task data for update: " + req.body)

    // Scan through array for the correct task based on ID
    while (tasksArray[i].Id != taskObject.Id)
        i++;

    console.log("Record in tasksArray to update: " + i)
    // replaces the existing task record
    tasksArray[i] = taskObject;

    // reply back to client with a success status
    res.status(200);

});

// POST call to delete an existing task in the system
toDoApp.post("/deletetask", function(req, res) {
    // taskIdToDelete stores the ID of the  task to be deleted
    var taskIdToDelete = req.body.Id;
    
    var i=0;

    console.log("Received task data for deletion: " + req.body)
    // Scan through array for the correct task based on ID and remove that task
    // while (tasksArray[i].Id != taskObject.Id)
    // [cheen888] replaced line 92 with line 94
    while (tasksArray[i].Id != taskIdToDelete) 
        i++;
    tasksArray.splice(i,1);
    // reply back to client with a success status
    res.status(200);
});

// standby function
// toDoApp.get("/gettask", function(req, res) {
    // taskObject.Id holds the ID of the  task being requested for
    // var taskObject.Id = req.body;
// });

// Custom Server 404 error handler
toDoApp.use(function(req, res) {
    res.send("<h1>Sorry. We don't have such a page.</h1>");
});

// Start toDoApp server at NODE_PORT
toDoApp.listen(NODE_PORT, function() {
    console.log("toDoApp server running, port: " + NODE_PORT);
});
